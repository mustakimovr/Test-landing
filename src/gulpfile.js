var gulp = require('gulp');
	concatCss = require('gulp-concat-css'),
	htmlmin = require('gulp-minify-html'),
	uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
	cleanCSS = require('gulp-clean-css');
 
gulp.task('default', function () {
  return gulp.src('css/*.css')
    .pipe(concatCss("style.min.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('assests/css'));
});

gulp.task('minify-html', function() {
  return gulp.src(['*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('assests'));
});

gulp.task('scripts', function() {
  return gulp.src('slick/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('assests/js'))
});

gulp.task('scripts-concat', function() {
  return gulp.src('slick/*.js')
    .pipe(concat('common.js'))
    .pipe(gulp.dest('assests/js'));
});

gulp.task('watch', function () {
	gulp.watch('css/*.css', ['default'])
});