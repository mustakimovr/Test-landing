$(window).scroll(function(){
  var docscroll=$(document).scrollTop();
  var width=$(document).width(); 
  if(docscroll>854){
    $('.nav').css({'height': "100%",'width': $('.nav').width()}).addClass('fixed');
  }else{
    $('.nav').removeClass('fixed');
  }
});
$(document).ready(function(){
	$("#home").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 1500);
	});

	$('#messege input[type=email]').on('blur', function () {
  let email = $(this).val();
  if (email.length > 0 && (email.match(/.+?\@.+/g) || []).length !== 1) {
    console.log('invalid');
    alert('Вы ввели некорректный e-mail! Поле должно содержать правильный email-адрес (например: example123@mail.ru) ');
  } else {
    console.log('valid');
  }

});
});

$(function() {
  $('input[type="submit"]').prop('disabled', true);
  $('input[name="email"]').keyup(function() {
    if($(this).val() != '') {             
      $('input[type="submit"]').prop('disabled', false);
    }
  });
});